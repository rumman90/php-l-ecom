<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/demo',function(){
    return "hellow";
});
// api 

Route::get('/all-category', [
    'uses'=>'AdminMasterController@all_Category',
    'as'  => '/all-category'
]);
Route::get('/all-product', 'ProductController@getAllProduct');
Route::get('/all-info-by-id/{id}', 'ProductController@allinfobyid');