@extends('master')
@section('homeContent')
<style>


    * {
        box-sizing: border-box;
    }

    .row {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        -ms-flex-wrap: wrap; /* IE10 */
        flex-wrap: wrap;
        margin: 0 -16px;
    }


    .col-50 {
        -ms-flex: 50%; /* IE10 */
        flex: 50%;
    }

    input[type=text] {
        align: center-block;
        width: 50%;
        margin-bottom: 20px;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 3px;
    }

    label {
        margin-bottom: 10px;
        display: block;
    }


</style><br><br>
<div class="container ">
    {!! Form::open(['url' =>'./save-shipping','method' => 'post','class'=>'cmxform form-horizontal']) !!}
    @csrf

    <div class="row">
        <div class="col-50">
            <h3>Shipping  Address</h3>
            <label for="fname"><i class="fa fa-user"></i> Full Name</label>
            <input type="text" id="fname" name="name"  value="">
            <input type="hidden"  name="id"  value="">
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="text" id="email" name="email" value="">
            <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
            <input type="text" id="adr" name="address" placeholder="542 W. 15th Street">
            <label for="city"><i class="fa fa-institution"></i> City</label>
            <input type="text" id="city" name="city" placeholder="New York">
            <input type="text" id="city" name="mobile" placeholder="mobile">

        </div>
    </div>
    <input type="submit" value="Continue to Shipping" class="btn">
    {!! Form::close() !!}

</div>
<br><br><br>


@endsection