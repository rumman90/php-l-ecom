
@extends('home')
@section('maincontent')
<div id="templatemo_content">

    <div class="post_section">

        <div class="post_date">
            30<span>Nov</span>
        </div>
        <div class="post_content">

            <h2>{{$blogInfo->blog_name}}</h2>

            <strong>Author:</strong> {{$blogInfo->author_name}} | <strong>Category:</strong> <a href="#">PSD</a>, <a href="#">Templates</a>

            <a href="#"><img src="{{asset($blogInfo->blog_image)}}" width="400" height="300" alt="Templates" /></a>

            <p>{{$blogInfo->blog_long_desc}}</p>





            @guest   
            <div id="comment_form">
                <h3>You Need to login for Comments & See comments. <a href="{{URL::to('/login')}}">Click To login</a> </h3>
            </div>
            @else
            
              <div id="comment_form">
                 <h3>Comment Here</h3>

                <form action="#" method="post">
                    <div class="form_row">
                        <label>Name ( Required )</label><br />
                        <input type="text" name="name" />
                    </div>
                    <div class="form_row">
                        <label>Email  (Required, will not be published)</label><br />
                        <input type="text" name="name" />
                    </div>
                    <div class="form_row">
                        <label>Your comment</label><br />
                        <textarea  name="comment" rows="" cols=""></textarea>
                    </div>

                    <input type="submit" name="Submit" value="Submit" class="submit_btn" />
                </form>

            </div>
            <div class="comment_tab">
                5 Comments
            </div>
            <div id="comment_section">
                <ol class="comments first_level">
                    <li>
                        <div class="comment_box commentbox1">
                            <div class="gravatar">
                                <img src="images/avator.png" alt="author 6" />
                            </div>
                            <div class="comment_text">
                                <div class="comment_author">Steven <span class="date">November 26, 2048</span> <span class="time">10:35 pm</span></div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dictum ornare nulla ac laoreet.</p>
                                <div class="reply"><a href="#">Reply</a></div>
                            </div>
                            <div class="cleaner"></div>
                        </div>                        
                    </li>
                    <li>
                        <ol class="comments">
                            <li>
                                <div class="comment_box commentbox2">
                                    <div class="gravatar">
                                        <img src="images/avator.png" alt="author 5" />
                                    </div>
                                    <div class="comment_text">
                                        <div class="comment_author">Julie <span class="date">November 27, 2048</span> <span class="time">09:20 pm</span></div>
                                        <p>Nullam bibendum tempor est nec cursus.</p>
                                        <div class="reply"><a href="#">Reply</a></div>
                                    </div>
                                    <div class="cleaner"></div>
                                </div>                        
                            </li>
                            <li>
                                <ol class="comments">
                                    <li>
                                        <div class="comment_box commentbox1">
                                            <div class="gravatar">
                                                <img src="images/avator.png" alt="author 4" />
                                            </div>
                                            <div class="comment_text">
                                                <div class="comment_author">John <span class="date">November 28, 2048</span> <span class="time">11:12 am</span></div>
                                                <p> Vestibulum eget ligula et ipsum laoreet aliquam sed ut risus.  </p>
                                                <div class="reply"><a href="#">Reply</a></div>
                                            </div>
                                            <div class="cleaner"></div>
                                        </div>                        
                                    </li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                </ol>
            </div>
            @endguest
        </div>

        <div class="cleaner"></div>

    </div>


</div>
@endsection