@extends('home')
@section('maincontent')


<?php
//$allblog = DB::table('tbl_blog')->where('publication_status', 1)->orderBy('id', 'desc')->get();
?>
<div id="templatemo_content">

    <div>
     {!! Form::open(['url' =>'./search_blog','method' => 'post']) !!}
                
        <input type="text" name="searchText">
        <input type="submit" name="btn" value="Search">
        {!!Form::close()!!}
    </div>

    @foreach($allblog as $blog)   
    <div class="post_section">
        <div class="post_date">
            30<span>Nov</span>
        </div>
        <div class="post_content">
            <h2><a href="{{URL::to('blog-details/'.$blog->id)}}">{{$blog->blog_name}}</a></h2>
            <strong>Author:</strong> {{$blog->author_name}} | <strong>Category:</strong> <a href="#"> {{$blog->author_name}}</a>, <a href="#">Templates</a>
            <a href="#"><img src="{{asset($blog->blog_image)}}"  width="500px" height="300px" alt="image" /></a>

            <p> {{$blog->blog_short_desc}}</p>
            <p><a href="blog_post.html">24 Comments</a> | <a href="blog_post.html">Continue reading...</a>                </p>
        </div>
        <div class="cleaner"></div>
    </div>
    @endforeach



</div>
@endsection