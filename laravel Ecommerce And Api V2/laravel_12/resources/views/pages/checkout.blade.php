@extends('master')
@section('homeContent')
<head>
    <link rel="stylesheet" href="{{asset('public/assets/frontEnd/')}}/css/form-elements.css">
    <link rel="stylesheet" href="{{asset('public/assets/frontEnd/')}}/css/fromstyle.css">

</head>
<div class="top-content">

    <div class="inner-bg">
        <div class="container">

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1>Customer Login &amp; Register Forms</h1>
                    <div class="description">
                        <p>

                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login to our site</h3>
                                <p>Enter username and password to log on:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form role="form" action="{{URL::to('/login_form')}}" method="post" class="login-form">
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Username</label>
                                    <input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password" required>
                                </div>
                                <button type="submit" class="btn">Sign in!</button>
                            </form>
                        </div>
                    </div>

                    <div class="social-login">
                        <h3>...or login with:</h3>
                        <div class="social-login-buttons">
                            <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                                <i class="fa fa-facebook"></i> Facebook
                            </a>
                            <a class="btn btn-link-1 btn-link-1-twitter" href="#">
                                <i class="fa fa-twitter"></i> Twitter
                            </a>
                            <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                                <i class="fa fa-google-plus"></i> Google Plus
                            </a>
                        </div>
                    </div>

                </div>

                <div class="col-sm-1 middle-border"></div>
                <div class="col-sm-1"></div>

                <div class="col-sm-5">

                    <div class="form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Sign up now</h3>
                                <p>Fill in the form below to get instant access:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="form-bottom">

                            {!! Form::open(['url' =>'./register_form','method' => 'post','class'=>'registration-form']) !!}
                            @csrf
                            <div class="form-group">
                                <label class="sr-only" for="form-first-name">Customer name</label>
                                <input type="text" name="name" placeholder="First name..." class="form-first-name form-control" id="form-first-name" required>
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-email">Email</label>
                                <input type="text" name="email" placeholder="Email..." class="form-email form-control" id="form-email" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Password</label>
                                <input type="password" name="password"  placeholder="password..." class="form-last-name form-control" id="form-last-name" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-last-name">Confarm Password</label>
                                <input type="password" name="conf_password" placeholder="confirm password..." equals="password" class="form-last-name form-control" id="form-last-name" >
                            </div>
                            <!--                                <div class="form-group">
                                                                <label class="sr-only" for="form-about-yourself">About yourself</label>
                                                                <textarea name="form-about-yourself" placeholder="About yourself..." 
                                                                          class="form-about-yourself form-control" id="form-about-yourself"></textarea>
                                                            </div>-->
                            <button type="submit" class="btn">Sign me up!</button>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>
@endsection