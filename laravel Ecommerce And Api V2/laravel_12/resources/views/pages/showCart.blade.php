@extends('master')
@section('homeContent')


<div class="page-head">
    <div class="container">
        <h3>Check Out</h3>
    </div>
</div>
<?php
$content = Cart::content();
//print_r($content);
?>
<!-- //banner -->
<!-- check out -->
<div class="checkout">
    <div class="container">
        <h3>My Shopping Bag</h3>
        <div class="table-responsive checkout-right animated wow slideInUp" data-wow-delay=".5s">
            <table class="timetable_sub">
                <thead>
                    <tr>
                        <th>Remove</th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Product Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                @foreach($content as $v_content)
                <tr class="rem2">
                    <td class="invert-closeb">
                        <a href="{{URL::to('/remove-to-cart/'.$v_content->rowId)}}" class="btn-danger">Remove</a>
                    </td>
                    <td class="invert-image"><a href="single.html"><img src="{{$v_content->options['image']}}" width="100" alt=" " class="img-responsive" /></a></td>
                    <td class="invert">
                        <div class="quantity"> 
                            <div class="quantity-select">                           
                                {!! Form::open(['url' =>'./update-cart','method' => 'post']) !!}
                                @csrf
                                <input type="text" name="qty" value="{{$v_content->qty}}">
                                <input type="hidden" name="rowId" value="{{$v_content->rowId}}">
                                <input type="submit" name="btn" value="Update">
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </td>
                    <td class="invert">{{$v_content->name}}</td>
                    <td class="invert">{{$v_content->price* $v_content->qty}}</td>

                </tr>
                @endforeach


            </table>
        </div>
        <div class="checkout-left">	

            <div class="checkout-right-basket animated wow slideInRight" data-wow-delay=".5s">
                <a href="{{URL::to('/')}}"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back To Shopping</a>
                <a href="{{URL::to('/checkout')}}"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Checkout</a>
            </div>
            <div class="checkout-left-basket animated wow slideInLeft" data-wow-delay=".5s">
                <h4>Shopping basket</h4>
                <ul>
                    <li>Subtotal <i>-</i> <span>{{Cart::subtotal()}}</span></li>
                    <li>Vat <i>-</i> <span>{{Cart::tax()}}</span></li>
                    <li>Shiping Charge <i>-</i> <span>Free</span></li>
                    <li>Total <i>-</i> <span>BDT:{{Cart::total()}}</span></li>

                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>	
@endsection