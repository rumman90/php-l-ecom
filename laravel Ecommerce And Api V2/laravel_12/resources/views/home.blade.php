<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Red Blog Theme - Free CSS Templates</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <link href="{{asset('public/assets/')}}/templatemo_style.css" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/css/app.css') }}" rel="stylesheet">
    </head>
    <body>

        <div id="templatemo_top_wrapper">
            <div id="templatemo_top">

                <div id="templatemo_menu">

                    <ul>
                        <li><a href="{{url('/')}}" class="current">Home</a></li>
                        <li><a href="{{url('/a')}}">Portfolio</a></li>
                        @guest
                        <li><a href="{{URL::to('/login')}}">Login</a></li>
                        <li><a href="{{URL::to('/register')}}">Signup</a></li>
                        @else
                        <li class="nav-item dropdown">
<!--                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>-->

<!--                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">-->
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
<!--                            </div>-->
                        </li>
                        @endguest
                    </ul>    	

                </div> <!-- end of templatemo_menu -->

                <div id="twitter">
                    <a href="#">follow us <br />
                        on twitter</a>
                </div>

            </div>
        </div>

        <div id="templatemo_header_wrapper">
            <div id="templatemo_header">

                <div id="site_title">
                    <h1><a href="#"><strong>Red Blog</strong><span>Free Blog Template in HTML CSS</span></a></h1>
                </div>

            </div>
        </div>

        <div id="templatemo_main_wrapper">
            <div id="templatemo_main">
                <div id="templatemo_main_top">

                    @yield('maincontent')




                    <div id="templatemo_sidebar">

                        <?php
                        $all_published_category = DB::table('categories')
                                        ->where('publication_status', 1)->get();
                        ?>
                        <h4>Categories</h4>
                        <ul class="templatemo_list">
                            @foreach($all_published_category as $category)
                            <li><a href="{{URL::to('/blog-category/'.$category->id)}}">{{$category->category_name}}</a></li>
                            @endforeach
                        </ul>

                        <div class="cleaner_h40"></div>
                        <?php
                        $latest_Blog = DB::table('tbl_blog')
                                ->where('publication_status', 1)
                                ->orderBy('id', 'desc')->limit(3)
                                ->get();
                        ?>
                        <h4>Letest Blog</h4>
                        <ul class="templatemo_list">
                            @foreach($latest_Blog as $latest)
                            <li><a href="{{URL::to('blog-details/'.$latest->id)}}">{{$latest->blog_name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="cleaner_h40"></div>

                        <?php
                        $popular_Blog = DB::table('tbl_blog')
                                ->where('publication_status', 1)
                                ->orderBy('hit_counter', 'desc')->limit(3)
                                ->get();
                        ?>
                        <h4>Popular Blog</h4>
                        <ul class="templatemo_list">
                            @foreach($popular_Blog as $popular)
                            <li><a href="{{URL::to('blog-details/'.$popular ->id)}}">{{$popular->blog_name}} (Hit:{{$popular->hit_counter}})</a></li>
                            @endforeach
                        </ul>

                        <!--                        <div id="ads">
                                                    <a href="#"><img src="{{asset('public/assets/')}}/images/templatemo_200x100_banner.jpg" alt="banner 1" /></a>
                                                    <a href="#"><img src="{{asset('public/assets/')}}/images/templatemo_200x100_banner.jpg" alt="banner 2" /></a>
                                                </div>-->

                    </div>

                    <div class="cleaner"></div>

                </div>

            </div>

            <div id="templatemo_main_bottom"></div>

        </div>

        <div id="templatemo_footer">

            Copyright © 2048 <a href="index.html">Your Company Name</a> <!-- Credit: www.templatemo.com -->| 
            Validate <a href="http://validator.w3.org/check?uri=referer">XHTML</a> &amp; 
            <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>

        </div>
        <!-- templatemo 264 red blog -->
        <!-- 
        Red Blog Template 
        http://www.templatemo.com/preview/templatemo_264_red_blog 
        -->
    </body>
</html>