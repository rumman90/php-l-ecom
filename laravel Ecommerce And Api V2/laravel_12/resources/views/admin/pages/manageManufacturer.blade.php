
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE widget-->
        <div class="widget purple">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Manage Manufacturer Table</h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div>
                    <div class="clearfix">

                        @if(session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                        @endif


                        <div class="btn-group">
                            <button id="editable-sample_new" class="btn green">
                                Add New <i class="icon-plus"></i>
                            </button>
                        </div>
                        <div class="btn-group pull-right">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Save as PDF</a></li>
                                <li><a href="#">Export to Excel</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Category Name</th>
                                <th>Category Description</th>
                                <th>Publication Status</th>
                                <th>Action</th>
<!--                                <th>Edit</th>
                                <th>Delete</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1; ?>
                            @foreach($all_Manufacturer as $viewManufacturer)
                            <tr class="">
                                <td><?php echo $count++; ?></td>
                                <td>{{$viewManufacturer->Manufacturer_name}}</td>
                                <td>{{$viewManufacturer->Manufacturer_desc}}</td>
                                <td>
                                    @if($viewManufacturer->publication_status==1)
                                    Published
                                    @else
                                    Unpublished
                                    @endif
                                </td>
                                <td class="center">
                                    @if($viewManufacturer->publication_status==1)
                                    <a href="{{URL::to('/unpublish_Manufacturer/'.$viewManufacturer->id)}}" class="btn btn-danger"><i class="icon-thumbs-down"></i></a>
                                    @else
                                    <a href="{{URL::to('/publish_Manufacturer/'.$viewManufacturer->id)}}" class="btn btn-success"><i class="icon-thumbs-up"></i></a>
                                    @endif
                                    <a href="{{URL::to('/edit_Manufacturer/'.$viewManufacturer->id)}}" class="btn btn-primary"><i class="icon-pencil"></i></a>
                                    <!-- delete sobar jonno na shudu admin er jonno
                                    ekhane user delete ta dekhte parbe na
                                    -->
                                    @if(Session::get('access_lavel')==1)
                                    <a href="{{URL::to('/delete_Manufacturer/'.$viewManufacturer->id)}}" onclick="return checkDelete()" class="btn btn-danger"><i class="icon-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
@endsection
