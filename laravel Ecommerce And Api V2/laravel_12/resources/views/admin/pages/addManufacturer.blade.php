
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> ADD Manufacturer</h4>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="widget-body form"> 
                
                @if(session('message'))
                <div class="alert alert-success">
                     {{session('message')}}
                </div>
                @endif
                
                <!-- BEGIN FORM-->
                <!--                <form class="cmxform form-horizontal" id="signupForm" method="post" action="{{url('./save-category')}}" novalidate="novalidate">
                -->
                {!! Form::open(['url' =>'./save-Manufacturer','method' => 'post','class'=>'cmxform form-horizontal']) !!}
                @csrf
                <div class="control-group ">
                    <label for="firstname" class="control-label">Manufacturer Name</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="manufacturerName" type="text">
                    </div>
                </div>
                <div class="control-group ">
                    <label for="lastname" class="control-label">Manufacture Description</label>
                    <div class="controls">
                        <input class="span6 " id="lastname" name="manufacturedesc" type="text">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Publication Status</label>
                    <div class="controls">
                        <select name="PublicationStatus" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">--Select Publication Status--- </option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>


                <div class="form-actions">
                    <button class="btn btn-success" type="submit">Save Manufacture</button>
                    <button class="btn" type="reset">Reset</button>
                </div>
                
                {!! Form::close() !!}
                <!--                </form>-->
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>

@endsection