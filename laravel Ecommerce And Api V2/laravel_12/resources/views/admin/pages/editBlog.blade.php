
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Advanced form Validation</h4>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="widget-body form"> 

                @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
                @endif

                <!-- BEGIN FORM-->
                <!--                <form class="cmxform form-horizontal" id="signupForm" method="post" action="{{url('./save-category')}}" novalidate="novalidate">
                -->
                {!! Form::open(['url' =>'./update-Blog','name'=>'editBlog','enctype'=>'multipart/form-data','method' => 'post','class'=>'cmxform form-horizontal']) !!}
                @csrf
                <div class="control-group ">
                    <label for="firstname" class="control-label">Blog Title</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="blogTitle" value="{{$blogInfo->blog_name}}" type="text">
                        <input class="span6 " name="blogid" value="{{$blogInfo->id}}" type="hidden">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Category </label>
                    <div class="controls">
                        <select name="category" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">---Select A category ---</option>
                            @foreach($CategoryInfo as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="control-group ">
                    <label for="firstname" class="control-label">Blog Image</label>
                    <div class="controls">
                        <input class="span4" id="firstname" name="Blogimage" type="file"><span><img src="{{asset($blogInfo->blog_image)}}" width="100" height="100"></span>
                        <input class="span4" value="{{$blogInfo->blog_image}}" name="OldImage" type="hidden">
                    </div>
                </div>

                <div class="control-group ">
                    <label for="lastname" class="control-label">Blog Short Description</label>
                    <div class="controls">
                        <textarea class="span6 " name="blogShortDesc"   rows="5">{{$blogInfo->blog_short_desc}}</textarea>
                    </div>
                </div>
                <div class="control-group ">
                    <label for="lastname" class="control-label">Blog Long Description</label>
                    <div class="controls">
                        <textarea class="span6 " name="blogLongDesc" rows="6">{{$blogInfo->blog_long_desc}}</textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Publication Status</label>
                    <div class="controls">
                        <select name="PublicationStatus" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">--Select Publication Status--- </option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>

                <div class="form-actions">
                    <button class="btn btn-success" type="submit">Update</button>
                    <button class="btn" type="button">Cancel</button>
                </div>

                {!! Form::close() !!}
                <!--                </form>-->
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>


<script type="text/javascript">
    document.forms['editBlog'].elements['PublicationStatus'].value = '<?php echo $blogInfo->publication_status; ?>'
    document.forms['editBlog'].elements['category'].value = '<?php echo $blogInfo->category_Id; ?>'
</script>

@endsection