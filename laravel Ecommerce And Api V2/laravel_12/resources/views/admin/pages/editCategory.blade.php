
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Advanced form Validation</h4>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="widget-body form"> 
                
                @if(session('message'))
                <div class="alert alert-success">
                     {{session('message')}}
                </div>
                @endif
                
                <!-- BEGIN FORM-->
                <!--                <form class="cmxform form-horizontal" id="signupForm" method="post" action="{{url('./save-category')}}" novalidate="novalidate">
                -->
                {!! Form::open(['url' =>'./update-category','method' => 'post','class'=>'cmxform form-horizontal']) !!}
                @csrf
                <div class="control-group ">
                    <label for="firstname" class="control-label">Category Name</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="categoryName" value="{{$categoryInfo->category_name}}" type="text">
                     <input class="span6 " name="categoryid" value="{{$categoryInfo->id}}" type="hidden">
                    </div>
                </div>
                <div class="control-group ">
                    <label for="lastname" class="control-label">Category Description</label>
                    <div class="controls">
                        <input class="span6 " id="lastname" value="{{$categoryInfo->category_desc}}" name="desc" type="text">
                    </div>
                </div>
                

                <div class="form-actions">
                    <button class="btn btn-success" type="submit">Update Category</button>
                    <button class="btn" type="button">Cancel</button>
                </div>
                
                {!! Form::close() !!}
                <!--                </form>-->
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>

@endsection