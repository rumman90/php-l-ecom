
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Add Product</h4>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                </div>
            </div>
            <div class="widget-body form"> 

                @if(session('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
                @endif

                <!-- BEGIN FORM-->
                <!--                <form class="cmxform form-horizontal" id="signupForm" method="post" action="{{url('./save-category')}}" novalidate="novalidate">
                -->
                {!! Form::open(['url' =>'./save-Product','enctype'=>'multipart/form-data','method' => 'post','class'=>'cmxform form-horizontal']) !!}
                @csrf
                <div class="control-group ">
                    <label for="firstname" class="control-label">Product Name</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="productName" type="text">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Category Name</label>
                    <div class="controls">
                        <select name="category" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">---Select A category ---</option>
                            @foreach($all_category as $category)
                            <option value="{{$category->id}}">{{$category->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Manufacturer Name</label>
                    <div class="controls">
                        <select name="manufacturer" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">---Select A Manufacturer ---</option>
                            @foreach($all_manufacturer as $manufacturer)
                            <option value="{{$manufacturer->id}}">{{$manufacturer->Manufacturer_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="control-group ">
                    <label for="firstname" class="control-label">Product Price</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="productPrice" type="text">
                    </div>
                </div>
                <div class="control-group ">
                    <label for="firstname" class="control-label">Product Quantity</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="productQuantity" type="text">
                    </div>
                </div>
                <div class="control-group ">
                    <label for="firstname" class="control-label">product Image</label>
                    <div class="controls">
                        <input class="span6 " id="firstname" name="productimage" type="file">
                    </div>
                </div>

                <div class="control-group ">
                    <label for="lastname" class="control-label">Product Short Description</label>
                    <div class="controls">
                        <textarea class="span6 " name="ProductShortDesc" rows="6"></textarea>
                    </div>
                </div>
                <div class="control-group ">
                    <label for="lastname" class="control-label">Product Long Description</label>
                    <div class="controls">
                        <textarea class="span6 " name="ProductLongDesc" rows="6"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Publication Status</label>
                    <div class="controls">
                        <select name="PublicationStatus" class="span6 chzn-select chzn-done" data-placeholder="Choose a Category" tabindex="-1" id="sel73Y" >
                            <option value="">--Select Publication Status--- </option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>
                </div>


                <div class="form-actions">
                    <button class="btn btn-success" type="submit">Save</button>
                    <button class="btn" type="button">Cancel</button>
                </div>

                {!! Form::close() !!}
                <!--                </form>-->
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>

@endsection