<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use Session;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

session_start();

class ProductController extends Controller {

    public function addProductPage() {
        $this->authCheck();
        $all_category = DB::table('categories')->get();
        $all_manufacturer = DB::table('manufacturers')->get();
        return view('admin.pages.addProduct')->with('all_category', $all_category)
                        ->with('all_manufacturer', $all_manufacturer);
    }

    public function saveProduct(Request $request) {

        //print_r($_FILES);
        $files = $request->file('productimage');
        $filename = $files->getClientOriginalName();
        $extention = $files->getClientOriginalExtension();
        $picture = date('His') . $filename;
        $image_Url = 'public/assets/Product_image/' . $picture;
        $destinationPath = base_path() . '/public/assets/Product_image/';
        $success = $files->move($destinationPath, $picture);
//------------------------------------------------------------------------------
        $data = array();
        $data['productName'] = $request->productName;
        $data['categoryId'] = $request->category;
        $data['manufacturerId'] = $request->manufacturer;
        $data['product_short_desc'] = $request->ProductShortDesc;
        $data['product_long_desc'] = $request->ProductLongDesc;
        $data['productPrice'] = $request->productPrice;
        $data['productQuantity'] = $request->productQuantity;
        $data['publication_status'] = $request->PublicationStatus;
        $data['created_at'] = Carbon::now();

        if ($success) {
            $data['product_image'] = $image_Url;
            DB::table('products')->insert($data);
            return redirect('/add-Product')->with("message", "Product Information save Successfully");
        } else {
            return redirect('/add-Product')->with("message", "Failed To save Product");
        }
    }

    public function manageProduct() {
        $all_Product = DB::table('products')->get(); // get dile all ashe       
        return view('admin.pages.manageProduct')->with('all_Product', $all_Product);
    }

    public function getAllProduct() {
        $all_Product = DB::table('products')->get(); // get dile all ashe       
        return $all_Product;
    }

    public function allinfobyid($Id) {
      return DB::table('products')->where('categoryId', $Id)->get();

    }

    public function unpublishProduct($productId) {
        DB::table('products')->where('productid', $productId)
                ->update(['publication_status' => 0]);
        return Redirect::to('/manage-Product')->with('message', "Product UnPublished Successfully");
    }

    public function publishProduct($productId) {
        DB::table('products')->where('productid', $productId)
                ->update(['publication_status' => 1]);
        return Redirect::to('/manage-Product')->with('message', "Product Published Successfully");
    }

    //auth and back buton
    public function authCheck() {
        $admin_id = Session::get('admin_id');
        if ($admin_id == NUll) {
            return Redirect::to('/admin')->send();
        }
    }

    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(product $product) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(product $product) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product $product) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(product $product) {
        //
    }

}
