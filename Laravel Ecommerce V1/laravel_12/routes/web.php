<?php

//Route::get('/', function () {
//    return view('welcome');
//});

Schema::defaultStringLength(191);

Route::get('/', 'WelcomeController@index');
Route::get('/a', 'WelcomeController@protfulio');
Route::get('/blog-details/{id}', 'WelcomeController@blogDetails');
Route::get('/blog-category/{id}', 'WelcomeController@blogPostByCategory');
Route::post('/search_blog', 'WelcomeController@searchBlog');

// ecommerce
Route::get('/product-details/{id}', 'WelcomeController@productDetails');
Route::get('/add-to-cart/{id}', 'CartController@addToCart');
Route::get('/show-cart', 'CartController@showCart');
Route::get('/remove-to-cart/{id}', 'CartController@removeToCart');
Route::post('/update-cart', 'CartController@updateCart');
Route::get('/empty-cart', 'CartController@emptyCart');
Route::get('/checkout', 'CartController@checkout');
Route::post('/register_form', 'CartController@registerform');
Route::get('/login_form', 'CartController@loginform');
Route::get('/logout-customer', 'CartController@logoutCustomer');
Route::get('/billing', 'CartController@billing');
Route::post('/update-Billing', 'CartController@updateBilling');
Route::get('/shipping', 'CartController@shipping');
Route::post('/save-shipping', 'CartController@Saveshipping');
Route::get('/payment', 'CartController@payment');
Route::post('/save-order', 'CartController@SaveOrder');


//for admin
Route::get('/admin', 'AdminController@index');
Route::post('/admin-login-check', 'AdminController@adminLoginCheck');
Route::get('/dashBord', 'AdminMasterController@index');
Route::get('/logout', 'AdminController@logout');


Route::get('/addCategory', 'AdminMasterController@addCategory');
Route::post('/save-category', 'AdminMasterController@saveCategory');
Route::get('/manage-category', 'AdminMasterController@manageCategory');
Route::get('/unpublish_category/{id}', 'AdminMasterController@unpublishCategory');
Route::get('/publish_category/{id}', 'AdminMasterController@publishCategory');
Route::get('/delete_category/{id}', 'AdminMasterController@deleteCategory');
Route::get('/edit_category/{id}', 'AdminMasterController@editCategory');
Route::post('/update-category', 'AdminMasterController@updateCategory');


Route::get('/addManufacturer', 'AdminMasterController@addManufacturer');
Route::post('/save-Manufacturer', 'AdminMasterController@saveManufacturer');
Route::get('/manage-Manufacturer', 'AdminMasterController@manageManufacturer');
Route::get('/unpublish_Manufacturer/{id}', 'AdminMasterController@unpublishManufacturer');
Route::get('/publish_Manufacturer/{id}', 'AdminMasterController@publishManufacturer');
Route::get('/delete_Manufacturer/{id}', 'AdminMasterController@deleteManufacturer');
Route::get('/edit_Manufacturer/{id}', 'AdminMasterController@editManufacturer');
Route::post('/update-Manufacturer', 'AdminMasterController@updateManufacturer');


Route::get('/add-Product', 'ProductController@addProductPage');
Route::post('/save-Product', 'ProductController@saveProduct');
Route::get('/manage-Product', 'ProductController@manageProduct');
Route::get('/unpublish-Product/{id}', 'ProductController@unpublishProduct');
Route::get('/publish-Product/{id}', 'ProductController@publishProduct');

Route::get('/addBlog', 'AdminMasterController@addBlog');
Route::post('/saveBlog', 'AdminMasterController@saveBlog');
Route::get('/manage-blog', 'AdminMasterController@manageBlog');
Route::get('/unpublish_Blog/{id}', 'AdminMasterController@unpublishBlog');
Route::get('/publish_Blog/{id}', 'AdminMasterController@publishBlog');
Route::get('/delete_Blog/{id}', 'AdminMasterController@deleteBlog');
Route::get('/edit_Blog/{id}', 'AdminMasterController@editBlog');
Route::post('/update-Blog', 'AdminMasterController@updateBlog');


Route::get('/mail', 'AdminMasterController@mail');

//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
