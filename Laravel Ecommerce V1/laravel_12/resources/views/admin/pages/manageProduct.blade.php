
@extends('admin.admin_master')
@section('content')
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN EXAMPLE TABLE widget-->
        <div class="widget purple">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Manage Blog Table</h4>
                <span class="tools">
                    <a href="javascript:;" class="icon-chevron-down"></a>
                    <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <div>
                    <div class="clearfix">
                        @if(session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                        @endif
                        <div class="btn-group">
                            <button id="editable-sample_new" class="btn green">
                                Add New <i class="icon-plus"></i>
                            </button>
                        </div>
                        <div class="btn-group pull-right">
                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Save as PDF</a></li>
                                <li><a href="#">Export to Excel</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>Serial</th>
                                <th>Product Name</th>
                                <th>Category Id</th>
                                <th>Manufacturer Id</th>
                                <th>Short Description</th>
                                <th>Long Description</th>                                
                                <th>Price</th>
                                <th>Quentity</th>
<!--                                <th>Image</th>-->
                                <th>Publication Status</th>
                                <th>Action</th>
<!--                                <th>Edit</th>
                                <th>Delete</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $count = 1; ?>
                            @foreach($all_Product as $viewproduct)
                            <tr class="">
                                <td><?php echo $count++; ?></td>
                                <td>{{$viewproduct->productName}}</td>
                                <td>{{$viewproduct->categoryId}}</td>
                                <td>{{$viewproduct->manufacturerId}}</td>
                                <td>{{$viewproduct->product_short_desc}}</td>
                                <td>{{$viewproduct->product_long_desc}}</td>
                                <td>{{$viewproduct->productPrice}}</td>
                                <td>{{$viewproduct->productQuantity}}</td>
<!--                                <td>{{$viewproduct->product_image}}</td>-->
                                <td>
                                    @if($viewproduct->publication_status==1)
                                    Published
                                    @else
                                    Unpublished
                                    @endif
                                </td>
                                <td class="center">
                                    @if($viewproduct->publication_status==1)
                                    <a href="{{URL::to('/unpublish-Product/'.$viewproduct->productid)}}" class="btn btn-danger"><i class="icon-thumbs-down"></i></a>
                                    @else
                                    <a href="{{URL::to('/publish-Product/'.$viewproduct->productid)}}" class="btn btn-success"><i class="icon-thumbs-up"></i></a>
                                    @endif
                                    <a href="{{URL::to('/edit-Product/'.$viewproduct->productid)}}" class="btn btn-primary"><i class="icon-pencil"></i></a>
                                    <!-- delete sobar jonno na shudu admin er jonno
                                    ekhane user delete ta dekhte parbe na
                                      @if(Session::get('access_lavel')==1)
                                         @endif
                                    -->

                                    <a href="{{URL::to('/delete-Product/'.$viewproduct->productid)}}" onclick="return checkDelete()" class="btn btn-danger"><i class="icon-trash"></i></a>

                                </td>
<!--                                <td><a class="edit" href="javascript:;">Edit</a></td>
                                <td><a class="delete" href="javascript:;">Delete</a></td>-->
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
@endsection
