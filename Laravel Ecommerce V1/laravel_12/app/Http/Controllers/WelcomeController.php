<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller {

    public function index() {
        $all_Product = DB::table('products')->where('publication_status', 1)->get();
        $home = view('pages.home')->with('all_Product', $all_Product);
        return view('master')->with('home', $home);
    }

    public function productDetails($id) {
        $productInfo = DB::table('products')->where('productid', $id)
                        ->where('publication_status', 1)->first();
        $productDetails = view('pages.productDetails')->with('productInfo', $productInfo);
        return view('master')->with('productDetails', $productDetails);
    }

    public function protfulio() {
        $protfulio = view('pages.protfulio');
        return view('home')->with('protfulio', $protfulio);
    }

    public function blogDetails($blogID) {
        $blogInfo = DB::table('tbl_blog')->where('id', $blogID)->first();

        // update and hit counter any item
        $data['hit_counter'] = $blogInfo->hit_counter + 1;
        DB::table('tbl_blog')->where('id', $blogID)->update($data);

        $blogDetails = view('pages.blogDetails')->with('blogInfo', $blogInfo);
        return view('home')->with('blogDetails', $blogDetails);
    }

    public function blogPostByCategory($categoryID) {

        $blogPostByCategory = DB::table('tbl_blog')
                ->where('publication_status', 1)
                ->where('category_Id', $categoryID)
                ->orderBy('id', 'desc')
                ->get();
        $blog = view('pages.blog')->with('allblog', $blogPostByCategory);
        return view('home')->with('blog', $blog);
    }

//    public function searchBlog(Request $request) {
//        $searchText = $request->searchText;
//        $search_blog = DB::table('tbl_blog')->where('publication_status', 1)
//                ->where('blog_name', 'like', '%' . $searchText . '%')
//                ->orderBy('id', 'desc')
//                ->get();
//        $blog = view('pages.Searchblog')->with('searchBlog', $search_blog);
//        return view('home')->with('blog', $blog);
//    }
}
