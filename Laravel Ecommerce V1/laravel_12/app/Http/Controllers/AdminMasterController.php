<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Category;
use Session;
use Illuminate\Support\Facades\Redirect;
use Mail;

session_start(); //back button security

class AdminMasterController extends Controller {

    public function index() {
        $this->authCheck();
        $dashBord = view('admin.pages.dashBord');
        return view('admin.admin_master')->with('dashBord', $dashBord);
    }

    public function addCategory() {
        return view('admin.pages.addCategory');
    }

    public function saveCategory(Request $request) {
        //$received=$request->all(); 
        //dd($received);//dd do or die
//=================Queary Builder (shofiul sir)=============
//        $data = array();
//        $data['category_name'] = $request->categoryName;
//        $data['category_desc'] = $request->desc;
//        $data['publication_status'] = $request->PublicationStatus;
//        $data['created_at'] = Carbon::now(); //laravel function date time
//        DB::table('categories')->insert($data);
//----------------model er object--------------------------
//        $data = new \App\Category();
//        $data['category_name'] = $request->categoryName;
//        $data['category_desc'] = $request->desc;
//        $data['publication_status'] = $request->PublicationStatus;
//        $data['created_at'] = Carbon::now();
//        $data->save();
//-----------------orm-------------------------------------
//orm use korle  
        //protected $fillable=['category_name','category_desc','publication_status','created_at'];
        //or nicher ta dite pari
        // protected $guarded = [''];
        $data = array();
        $data['category_name'] = $request->categoryName;
        $data['category_desc'] = $request->desc;
        $data['publication_status'] = $request->PublicationStatus;
        $data['created_at'] = Carbon::now();
        Category::create($data);
        return redirect('/addCategory')->with("message", "Category Save Successfully");
    }

    public function manageCategory() {
        $all_category = DB::table('categories')->get(); // get dile all ashe       
        return view('admin.pages.manageCategory')->with('all_category', $all_category);
    }

    public function unpublishCategory($categoryId) {
        DB::table('categories')->where('id', $categoryId)
                ->update(['publication_status' => 0]);
        return Redirect::to('/manage-category');
    }

    public function publishCategory($categoryId) {
        DB::table('categories')->where('id', $categoryId)
                ->update(['publication_status' => 1]);
        return Redirect::to('/manage-category');
    }

    public function deleteCategory($categoryId) {
        DB::table('categories')->where('id', $categoryId)
                ->delete();
        return Redirect::to('/manage-category');
    }

    public function editCategory($categoryId) {

        $categoryInfo = DB::table('categories')->where('id', $categoryId)
                ->first();
        return view('admin.pages.editCategory')->with('categoryInfo', $categoryInfo);
    }

    public function updateCategory(Request $request) {
        $data = array();
        $categoryid = $request->categoryid;
        $data['category_name'] = $request->categoryName;
        $data['category_desc'] = $request->desc;
        $data['updated_at'] = Carbon::now(); //laravel function date time
        DB::table('categories')->where('id', $categoryid)
                ->update($data);

        return Redirect::to('/manage-category');
    }

//=======================================================================================
//==============================Manufacturer=============================================
//=======================================================================================


    public function addManufacturer() {
        return view('admin.pages.addManufacturer');
    }

    public function saveManufacturer(Request $request) {
//=================Queary Builder (shofiul sir)=============
        $data = array();
        $data['Manufacturer_name'] = $request->manufacturerName;
        $data['Manufacturer_desc'] = $request->manufacturedesc;
        $data['publication_status'] = $request->PublicationStatus;
        $data['created_at'] = Carbon::now(); //laravel function date time
        DB::table('manufacturers')->insert($data);
        return redirect('/addManufacturer')->with("message", "Manufacturer Save Successfully");
    }

    public function manageManufacturer() {
        $all_Manufacturer = DB::table('manufacturers')->get(); // get dile all ashe       
        return view('admin.pages.manageManufacturer')->with('all_Manufacturer', $all_Manufacturer);
    }

    public function unpublishManufacturer($manufacturerId) {
        DB::table('manufacturers')->where('id', $manufacturerId)
                ->update(['publication_status' => 0]);
        return Redirect::to('/manage-Manufacturer')->with("message", "Manufacturer Unpublished  Successfully");
    }

    public function publishManufacturer($manufacturerId) {
        DB::table('manufacturers')->where('id', $manufacturerId)
                ->update(['publication_status' => 1]);
        return Redirect::to('/manage-Manufacturer')->with("message", "Manufacturer published  Successfully");
    }

    public function deleteManufacturer($manufacturerId) {
        DB::table('manufacturers')->where('id', $manufacturerId)
                ->delete();
        return Redirect::to('/manage-Manufacturer')->with("message", "Manufacturer Delete  Successfully");
    }

    public function editManufacturer($manufacturerId) {
        $manufacturerInfo = DB::table('manufacturers')->where('id', $manufacturerId)
                ->first();
        return view('admin.pages.editManufacturer')->with('manufacturerInfo', $manufacturerInfo);
    }

    public function updateManufacturer(Request $request) {
        $data = array();
        $manufacturerid = $request->manufacturerid;
        $data['Manufacturer_name'] = $request->manufacturerName;
        $data['Manufacturer_desc'] = $request->manufacturedesc;
        $data['publication_status'] = $request->PublicationStatus;
        $data['updated_at'] = Carbon::now(); //laravel function date time
        DB::table('manufacturers')->where('id', $manufacturerid)
                ->update($data);
        return Redirect::to('/manage-Manufacturer')->with("message", "Manufacturer Update  Successfully");
        ;
    }

//===================================================================================
//===========================Blog==================== ==============================
    public function addBlog() {
        $all_category = DB::table('categories')->get();
        return view('admin.pages.addBlog')->with('all_category', $all_category);
    }

    public function saveBlog(Request $request) {
        //print_r($_FILES);
        $files = $request->file('Blogimage');
        $filename = $files->getClientOriginalName();
        $extention = $files->getClientOriginalExtension();
        $picture = date('His') . $filename;
        $image_Url = 'public/assets/blog_image/' . $picture;
        $destinationPath = base_path() . '/public/assets/blog_image/';
        $success = $files->move($destinationPath, $picture);
//------------------------------------------------------------------------------
        $data = array();
        $data['category_Id'] = $request->category;
        $data['blog_name'] = $request->blogTitle;
        $data['blog_short_desc'] = $request->blogShortDesc;
        $data['blog_long_desc'] = $request->blogLongDesc;
        $data['author_name'] = Session::get('admin_name');
        $data['publication_status'] = $request->PublicationStatus;
        $data['created_at'] = Carbon::now();

        if ($success) {
            $data['blog_image'] = $image_Url;
            DB::table('tbl_blog')->insert($data);
            return redirect('/addBlog')->with("message", "Blog Information save Successfully");
        } else {
            return redirect('/addBlog')->with("message", "Blog Information save UnSuccessfully");
        }
    }

    public function manageBlog() {
        $all_Blog = DB::table('tbl_blog')->get(); // get dile all ashe       
        return view('admin.pages.manageBlog')->with('all_Blog', $all_Blog);
    }

    public function unpublishBlog($blogid) {
        DB::table('tbl_blog')->where('id', $blogid)
                ->update(['publication_status' => 0]);
        return Redirect::to('/manage-blog');
    }

    public function publishBlog($blogid) {
        DB::table('tbl_blog')->where('id', $blogid)
                ->update(['publication_status' => 1]);
        return Redirect::to('/manage-blog');
    }

    public function deleteBlog($blogid) {
        DB::table('tbl_blog')->where('id', $blogid)
                ->delete();
        return Redirect::to('/manage-blog');
    }

    public function editBlog($blogid) {
        $BlogInfo = DB::table('tbl_blog')->where('id', $blogid)
                ->first();
        $CategoryInfo = DB::table('categories')->get();
        return view('admin.pages.editBlog')->with('blogInfo', $BlogInfo)->with('CategoryInfo', $CategoryInfo);
    }

    public function updateBlog(Request $request) {


//------------------------------------------------------------------------------
        $data = array();
        $blogid = $request->blogid;
        $data['category_Id'] = $request->category;
        $data['blog_name'] = $request->blogTitle;
        $data['blog_short_desc'] = $request->blogShortDesc;
        $data['blog_long_desc'] = $request->blogLongDesc;
        $data['publication_status'] = $request->PublicationStatus;
        $data['updated_at'] = Carbon::now(); //laravel function date time
        //----------------------------image Upload--------------------------------------
        $files = $request->file('Blogimage');


        if ($files) {
            $filename = $files->getClientOriginalName();
            //$extention = $files->getClientOriginalExtension();
            $picture = date('His') . $filename;
            $image_Url = 'public/assets/blog_image/' . $picture;
            $destinationPath = base_path() . '/public/assets/blog_image/';
            $success = $files->move($destinationPath, $picture);

            if ($success) {
                $data['blog_image'] = $image_Url;
                DB::table('tbl_blog')->where('id', $blogid)
                        ->update($data);
                Session::put('message', 'Blog Information Update Successfully');
                //purber image ke foolder theke delete kore dey
                if ($request->OldImage != NULL) {
                    unlink($request->OldImage);
                }
                return Redirect::to('/manage-blog');
            }
        } else {
            $data['blog_image'] = $request->OldImage;
            DB::table('tbl_blog')->where('id', $blogid)
                    ->update($data);
            Session::put('message', 'Blog Information Update Successfully');
            return Redirect::to('/manage-blog');
        }
    }

    public function mail() {
        $data = array(
            'name' => 'rumman',
            'email' => 'rumman821090@gmail.com',
        );
        Mail::send('pages.mail', $data, function ($message) {
            $message->to('rummanbabu544@gmail.com');
            $message->subject('subject');
        });
    }

    public function authCheck() {
        $admin_id = Session::get('admin_id');
        if ($admin_id == NUll) {
            return Redirect::to('/admin')->send();
        }
    }

}
