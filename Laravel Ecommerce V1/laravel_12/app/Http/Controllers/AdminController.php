<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;

session_start();

class AdminController extends Controller {

    public function index() {
        $this->authCheck();
        return view('admin.admin_login');
    }

    public function adminLoginCheck(Request $request) {
        $email_address = $request->email;
        $password = $request->password;

        $result = DB::table('admin')
                ->select('*')
                ->where('admin_email', $email_address)
                ->where('password', md5($password))
                ->first();
        // dd($result);
        if ($result) {
            Session::put('admin_name', $result->admin_name);
            Session::put('admin_id', $result->id);
            Session::put('access_lavel', $result->access_lavel);
            return Redirect::to('/dashBord');
        } else {
            Session::put('admin_check', "Your User id or password Invalide");
            return Redirect::to('/admin');
        }
    }

    public function logout() {
        Session::put('admin_name', '');
        Session::put('admin_id', '');
        Session::put('message', 'You are Successfuly Logout');
        return Redirect::to('/admin');
    }

    //auth and back buton
    public function authCheck() {
        $admin_id = Session::get('admin_id');
        if ($admin_id != NUll) {
            return Redirect::to('/dashBord')->send();
        }
    }

    public function create() {
        //
    }

    public function store(Request $request) {
        //
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }

}
