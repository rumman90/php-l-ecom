<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use DB;
use Illuminate\Support\Carbon;
use Session;

class CartController extends Controller {

    public function addToCart($productid) {
        $productInfo = DB::table('products')
                        ->select('*')
                        ->where('productid', $productid)
                        ->where('publication_status', 1)->first();
        $data = array();
        $data['id'] = $productInfo->productid;
        $data['name'] = $productInfo->productName;
        $data['qty'] = 1;
        $data['price'] = $productInfo->productPrice;
        $data['options']['image'] = $productInfo->product_image;
        Cart::add($data);
        return \Illuminate\Support\Facades\Redirect::to('/');
    }

    public function showCart() {
        return view('pages.showCart');
    }

    public function removeToCart($rowid) {
        Cart::remove($rowid);
        return \Illuminate\Support\Facades\Redirect::to('/show-cart');
    }

    public function updateCart(Request $request) {
        $qty = $request->qty;
        $rowId = $request->rowId;
        Cart::update($rowId, $qty);
        return \Illuminate\Support\Facades\Redirect::to('/show-cart');
    }

    public function emptyCart() {
        Cart::destroy();
        return \Illuminate\Support\Facades\Redirect::to('/');
    }

    public function checkout() {
        return view('pages.checkout');
    }

    public function registerform(Request $request) {
        $data = array();
        $data['cus_name'] = $request->name;
        $data['cus_email'] = $request->email;
        $data['cus_password'] = $request->password;
        $data['created_at'] = Carbon::now(); //laravel function date time
        $custorerId = DB::table('customer')->insertGetId($data); //last id ta return kore

        Session::put('custorerId', $custorerId);
        Session::put('customerName', $request->name);
        return redirect('/billing');
    }

    public function billing() {
        $custorerIdRec = Session::get('custorerId');
        $customerinfo = DB::table('customer')->where('cus_id', $custorerIdRec)->first();
        return view('pages.billing')->with('customerinfo', $customerinfo);
    }

    public function updateBilling(Request $request) {
        $data = array();
//        $data['cus_name'] = $request->name;
//        $data['cus_email'] = $request->email;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['mobile'] = $request->mobile;
        $data['updated_at'] = Carbon::now(); //laravel function date time

        $custorerId = DB::table('customer')->where('cus_id', $request->cusid)
                ->update($data);
        return redirect('/shipping');
    }

    public function shipping() {
        return view('pages.shipping');
    }

    public function Saveshipping(Request $request) {
        $data = array();
        $data['shi_name'] = $request->name;
        $data['shi_email'] = $request->email;
        $data['address'] = $request->address;
        $data['city'] = $request->city;
        $data['mobile'] = $request->mobile;
        $data['created_at'] = Carbon::now();
        $shippingID= DB::table('shipping')->insertGetId($data); //last id ta return kore

        Session::put('shippingID', $shippingID);
        return redirect('/payment');
    }

    public function payment(){
          return view('pages.payment');
    }
    
    public function SaveOrder(Request $request){
        $payment_type=$request->paymentType; 
        if($payment_type=='cashOnd'){
            
        }if($payment_type=='paypal'){
            return view('pages.htmlwebsitestandardpayments');
        }
    }

    public function logoutCustomer() {
        Session::put('custorerId', '');
        Session::put('customerName', '');
        return redirect('/');
    }

    public function create() {
        //
    }

    public function store(Request $request) {
        //
    }

    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
