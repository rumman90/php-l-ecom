<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('cus_id');
            $table->string('cus_name');
            $table->string('cus_email')->unique();
            $table->string('cus_password');
            $table->text('address')->nullable();;
            $table->string('city')->nullable();;
            $table->Integer('mobile')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('customer');
    }

}
