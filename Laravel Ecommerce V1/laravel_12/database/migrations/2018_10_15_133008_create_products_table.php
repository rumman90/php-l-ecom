<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('productid');
            $table->string('productName');
            $table->integer('categoryId');
            $table->integer('manufacturerId');
            $table->text('product_short_desc');
            $table->text('product_long_desc');
            $table->string('productPrice');
            $table->string('productQuantity');
            $table->string('product_image');
            $table->boolean('publication_status');
            //           $table->integer('hit_counter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('products');
    }

}
