<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up() {
        Schema::create('tbl_blog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_Id');
            $table->string('blog_name');
            $table->text('blog_short_desc');
            $table->text('blog_long_desc');
            $table->string('author_name');
            $table->string('blog_image');
            $table->boolean('publication_status');
            $table->integer('hit_counter');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tbl_blog');
    }

}
