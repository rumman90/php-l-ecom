<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('shipping', function (Blueprint $table) {
            $table->increments('shi_id');
            $table->string('shi_name');
            $table->string('shi_email');
            $table->text('address');
            $table->string('city');      
            $table->Integer('mobile') ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('shipping');
    }

}
